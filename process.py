#!/usr/bin/env python3

'''
@author: Piotr Nikiel
@date: Oct 2015

@desc: This tool serves to deliver an overview of different quasar-based servers out there.
In fact it is a trivially simple "database" (source contents in repo_paths.txt) with a python
script capable of exporting interesting information as a HTML webpage.

'''

import csv
import os
import pdb
from lxml import objectify
import argparse
from yattag import Doc, indent
from datetime import datetime
import shutil
import glob
import random
import pygit2

EmbeddedCss = """
    body
    {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-width: 10px;
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid #bbb;
        padding: 4px;
    }
    th
    {
        background-color: #4CAF50;
        color: white;
        text-align: left;
        padding-top: 12px;
        padding-bottom: 12px;
    }
    #tr:nth-child(even){
    #    background-color: #f2f2f2;
    #}
    .tr_phase
    {
        background-color: #bce3be;
        text-align: center;
        font-style: italic;
    }
    """

QuasarNameSpaces={'d':'http://cern.ch/quasar/Design'}
CountedElements = [
    'class',
    'cachevariable',
    'sourcevariable',
    'configentry',
    'method',
    'array',
    'object',
    'documentation']

def describe_submodule_head(main_repo, submodule_name):
    try:
        repo = pygit2.Repository(submodule_name)
        return repo.describe(describe_strategy=pygit2.GIT_DESCRIBE_TAGS)
    except Exception as exc:
        return f'Error: {exc}'

class Server():
    def __init__(self, name, clone_url, repo_type):
        self.name = name
        self.clone_url = clone_url
        self.repo_type = repo_type
        self.cloned_dir = None
        self.download()

    def download(self):
        try:
            shutil.rmtree('tmp')
        except:
            pass
        if self.repo_type == 'svn':
            os.system('svn export {0} tmp'.format(self.clone_url))
        elif self.repo_type == 'git':
            os.system('git clone --depth 1 {0} tmp'.format(self.clone_url))
        else:
            raise Exception('invalid repo type:'+repo_type)
        self.cloned_dir = 'tmp'

    def get_default_endpoint_url(self):
        try:
            server_config_xml = objectify.parse(os.path.join(self.cloned_dir, 'bin', 'ServerConfig.xml'))
            default_endpoint_node = server_config_xml.xpath('/OpcServerConfig/UaServerConfig/UaEndpoint/Url')
            if len(default_endpoint_node) < 1:
                default_endpoint_node = server_config_xml.xpath('/sc:OpcServerConfig/sc:UaServerConfig/sc:UaEndpoint/sc:Url', namespaces = {'sc' : 'http://cern.ch/quasar/ServerConfig'})
            default_endpoint = default_endpoint_node[0].text.replace('opc.tcp://[NodeName]:', '') # remove clutter for default NodeName
            return default_endpoint
        except:
            return "<b>ERROR</b>"

    def get_git_submodules(self):
        git_submodules = []
        orig_dir = os.getcwd()
        print(f'Getting git submodules. Starting in {orig_dir} ')
        os.chdir(self.cloned_dir)
        os.system('git submodule init')
        os.system('git submodule update')
        repo = pygit2.Repository('.')
        declared_submodules = [x for x in repo.listall_submodules() if not 'FrameworkInternals' in x]
        git_submodules.extend([(x, describe_submodule_head(repo, x), 'git submodule') for x in declared_submodules])
        os.chdir(orig_dir)
        return git_submodules

class NonQuasarOpcUaServer(Server):
    def __init__(self, name, clone_url, repo_type):
        Server.__init__(self, name, clone_url, repo_type)
        self.optional_modules = self.get_git_submodules()
        self.element_counts = {elm : '?' for elm in CountedElements }

class QuasarOpcUaServer(Server):
    def __init__(self, name, clone_url, repo_type):
        Server.__init__(self, name, clone_url, repo_type)
        self.quasar_version = self.get_file_contents('Design/quasarVersion.txt')
        self.xml_tree = objectify.parse(os.path.sep.join([self.cloned_dir, 'Design', 'Design.xml']))
        self.element_counts = { what: self.count_design_elements(what) for what in CountedElements }
        self.optional_modules = self.get_optional_modules() + self.get_git_submodules()

    def get_file_contents(self, file_name):
        '''in fact, for now, only returns the very first line ... '''
        a_file = open(os.path.sep.join([self.cloned_dir, file_name]), 'r')
        return a_file.readline()

    def count_design_elements(self, what):
        r = self.xml_tree.xpath('//d:'+what, namespaces = QuasarNameSpaces)
        return len(r)

    def get_optional_modules(self):
        '''Returns a list of tuples (name, version)'''
        # 1st part: look for modules deployed using quasar's enable_module ...
        tag_file_names = glob.glob(self.cloned_dir+'/FrameworkInternals/EnabledModules/*.tag')
        optional_modules = [
            (os.path.basename(x).replace('.tag',''),
            open(x, 'r').readline().rstrip(),
            'build-time module')
            for x in tag_file_names]
        return optional_modules



parser = argparse.ArgumentParser()
parser.add_argument('--test', action='store_true')
parser.add_argument('--test_with', default='ATLAS_SCA')
args = parser.parse_args()



def get_repo_paths():
    result = []
    with open('repo_paths.txt', 'r') as csvfile:
        paths_reader =  csv.reader(csvfile, delimiter=' ', skipinitialspace=True)
        for row in paths_reader:
            if len(row)>0:
                result.append({'name':row[0], 'phase':row[1], 'type':row[2], 'path':row[3]})
    return result


def translate_phase(phase):
    if phase == 'prod':
        return 'Production'
    elif phase == 'dev':
        return 'Development'
    elif phase == 'hist':
        return 'Historic projects (retired, deprecated, cancelled)'
    else:
        return phase

def write_repos(repositories, tag, text, line):
    for repo in repositories:
        print('Now doing: {0}'.format(repo['name']))
        with tag('tr'):
            with tag('td'): text(repo['name'])
            try:
                server = QuasarOpcUaServer(repo['name'], repo['path'], repo['type'])
                with tag('td'): text(server.quasar_version)
            except:
                server = NonQuasarOpcUaServer(repo['name'], repo['path'], repo['type'])
                with tag('td'): text('Non-quasar')

            with tag('td'):
                with tag('ul'):
                    for optional_module in server.optional_modules:
                        line('li', "{0} (ver: {1}, {2})".format(optional_module[0], optional_module[1], optional_module[2]))
            with tag('td'): text(server.get_default_endpoint_url())
            for counted_element in CountedElements:
                count = server.element_counts[counted_element]
                count_and_suppress_zero = '' if count == 0 else str(count)
                with tag('td'): text(count_and_suppress_zero)

def main():
    repositories = get_repo_paths()

    if args.test:
        repositories = [repo for repo in repositories if repo['name'] == args.test_with]

    doc, tag, text, line = Doc().ttl()
    with tag('html'):
        with tag('head'):
            with tag('title'): text("Summary of known quasar-based OPC-UA servers")
            #doc.stag('link', rel='stylesheet', href='quasar_opcua_servers.css')
            doc.asis(f'<style>{EmbeddedCss}</style>\n\n')
            doc.asis('<script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>')

        with tag('p'):
            with tag('i'):
                text('Generated on {0}'.format(datetime.now()))
            with tag('p'):
                text('The table is sortable (click on headers!)')

        with tag('table', klass='sortable'):
            # table header.
            with tag('thead'):
                with tag('tr'):
                    with tag('th'): text('Server name')
                    with tag('th'): text('quasar version')
                    with tag('th'): text('optional modules (non-exhaustive)')
                    with tag('th'): text('dflt endpoint')
                    for counted_element in CountedElements:
                        with tag('th'): text('#' + counted_element)

            # actual table body, i.e. valuable content.
            phases = ['prod', 'dev', 'hist']
            for phase in phases:
                with tag('tr', klass='tr_phase'):
                    with tag('td', colspan="100%"):
                        text(f'Development phase: {translate_phase(phase)}')
                repos = [ repo for repo in repositories if repo['phase'] == phase ]
                write_repos(repos, tag, text, line)

    out_file = open('quasar_opcua_servers.html','w')
    out_file.write(indent(doc.getvalue()))
    out_file.close()

main()
